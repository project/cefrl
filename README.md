CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

This module provides a new field type with levels of language proficiency as
 defined by the Common European Framework of Reference for Languages
 ([more on Wikipedia](https://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages)),
 available as select options.

Features include:

 * ability to select groups of levels in addition to individual levels;
 * ability to select a pseudo-level for Native speakers of a language;
 * configurable labels and descriptions of the levels and groups;
 * a Views filter for levels that allows less than / between / greater than comparisons.


For a full description of the module, visit the project page:
   https://www.drupal.org/project/cefrl

To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/cefrl

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

 * [Custom Language field](https://www.drupal.org/project/languagefield):
   Useful module providing configurable languages, which can be paired neatly
   with language levels.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Configure the user permissions in _Administration » People » Permissions_:

   - Administer CEFRL definitions

     Users with this permission will be able to configure the labels and
     descriptions of all the CEFRL levels and groups.

 * Configure the labels and descriptions of all the CEFRL levels and groups in
   _Administration » Configuration » User interface » CEFRL definitions_.
