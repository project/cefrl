<?php

namespace Drupal\Tests\cefrl\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group cefrl
 */
class CEFRLLevelFormatterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'cefrl'
  ];

  /**
   * A user with permission to create articles.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    $this->drupalCreateContentType(['type' => 'article']);
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($this->webUser);

    // Add the telephone field to the article content type.
    FieldStorageConfig::create([
      'field_name' => 'field_cefrl',
      'entity_type' => 'node',
      'type' => 'cefrl',
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_cefrl',
      'label' => 'CEFRL',
      'entity_type' => 'node',
      'bundle' => 'article',
      'settings' => [
        'allow_group_selection' => TRUE,
        'include_native_speaker' => TRUE,
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent('field_cefrl', [
        'type' => 'cefrl',
      ])
      ->save();
    $display_repository->getViewDisplay('node', 'article')
      ->setComponent('field_cefrl', [
        'type' => 'cefrl_default',
        'weight' => 1,
      ])
      ->save();
  }

  /**
   * Tests the CEFRL default formatter.
   *
   * @covers \Drupal\cefrl\Plugin\Field\FieldFormatter\CEFRLLevelDefaultFormatter::viewElements
   *
   * @dataProvider providerDefaultOptions
   */
  public function testDefaultFormatter($input, $expected) {
    // Test basic entry of CEFRL field.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'field_cefrl[0][level]' => $input,
    ];

    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $this->assertSession()
      ->responseContains($expected);
  }

    /**
   * Provides the CEFRL default options to check and expected results.
   */
  public function providerDefaultOptions() {
    return [
      'group as option' => ['B', 'B - Independent user'],
      'level as option' => ['B1', 'B1 - Threshold'],
      'pseudo-level as option' => ['NS', 'Native speaker'],
    ];
  }

  /**
   * Tests the CEFRL description formatter.
   *
   * @covers \Drupal\cefrl\Plugin\Field\FieldFormatter\CEFRLLevelDescriptionFormatter::viewElements
   *
   * @dataProvider providerDescriptionOptions
   */
  public function testDescriptionFormatter($input, $expected) {
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getViewDisplay('node', 'article')
      ->setComponent('field_cefrl', [
        'type' => 'cefrl_description',
        'weight' => 1,
      ])
      ->save();

    // Test basic entry of CEFRL field.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'field_cefrl[0][level]' => $input,
    ];

    $this->drupalGet('node/add/article');
    $this->submitForm($edit, 'Save');
    $this->assertSession()
      ->responseContains('<summary>' . $expected['label'] . '</summary>');
    $this->assertSession()
      ->responseContains($expected['description']);
  }

    /**
   * Provides the CEFRL description options to check and expected results.
   */
  public function providerDescriptionOptions() {
    return [
      'group as option' => ['C', [
        'label' => 'C - Proficient user',
        'description' => ''
      ]],
      'level as option' => ['C1', [
        'label' => 'C1 - Advanced',
        'description' => 'Can understand a wide range of demanding, longer clauses and recognise implicit meaning.<br />'
      ]],
    ];
  }

}
