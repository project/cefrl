<?php

namespace Drupal\Tests\cefrl\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Covers the CEFRL settings form for labels and definitions.
 *
 * @group cefrl
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'cefrl'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Tests that the settings form is acessible only by a privileged user.
   */
  public function testSettingsFormAccess() {
    $account = $this->drupalCreateUser(['administer cefrl configuration']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/config/user-interface/cefrl');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogout();

    $this->drupalGet('admin/config/user-interface/cefrl');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the interaction between the settings form and the configuration.
   */
  public function testSettingsFormConfigurationChanges() {
    // Test the default configuration.
    $default_config = $this->config('cefrl.settings')->get('definitions');

    $this->assertEquals('A', $default_config[0]['id']);
    $this->assertEquals('Basic user', $default_config[0]['label']);
    $this->assertEmpty($default_config[0]['description']);

    // Test the form contains the default configuration.
    $account = $this->drupalCreateUser(['administer cefrl configuration']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/config/user-interface/cefrl');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertEquals('A1', $default_config[1]['id']);
    $this->assertSession()->fieldValueEquals('edit-a1-label', 'Breakthrough');

    $this->assertEquals('A2', $default_config[2]['id']);
    $this->assertSession()->elementContains('css', '#edit-a2-description', 'frequently used expressions');

    // Test form submission.
    $input = [];
    $input['edit-b-description'] = 'Can into independence.';
    $input['edit-b1-label'] = 'Threshold (early independent)';
    $input['edit-b2-description'] = '';

    $this->submitForm($input, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Test the configuration has been updated.
    $new_config = $this->config('cefrl.settings')->get('definitions');
    $this->assertNotEquals($default_config, $new_config);

    $this->assertEquals('Can into independence.', $new_config[3]['description']);
    $this->assertEquals('Threshold (early independent)', $new_config[4]['label']);
    $this->assertEmpty($new_config[5]['description']);

    // Test the new values are there.
    $this->drupalGet('admin/config/user-interface/cefrl');

    $this->assertSession()->fieldValueEquals('edit-b-description', 'Can into independence.');
    $this->assertSession()->fieldValueEquals('edit-b1-label', 'Threshold (early independent)');
    $this->assertSession()->fieldValueEquals('edit-b2-description', '');
  }

    /**
   * Tests that the settings form is acessible only by a privileged user.
   */
  public function testSettingsFormBehaviors() {
    $account = $this->drupalCreateUser(['administer cefrl configuration']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/config/user-interface/cefrl');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('edit-c-label', 'Proficient user');
    $this->assertSession()->elementAttributeNotExists('css', '#edit-c', 'open');

    // Test form submission with empty labels.
    $input = [];
    $input['edit-c1-label'] = '';
    $input['edit-c2-label'] = '';
    $input['edit-c2-description'] = '';
    $input['edit-ns-label'] = '';

    $this->submitForm($input, 'Save configuration');

    // Details element should be open when label is empty.
    $this->assertSession()->elementAttributeExists('css', '#edit-c1', 'open');
    $this->assertSession()->elementAttributeExists('css', '#edit-c2', 'open');

    // Native speaker option should revert to default when saved as empty.
    $this->assertSession()->elementAttributeNotExists('css', '#edit-ns', 'open');
    $this->assertSession()->fieldValueEquals('edit-ns-label', 'Native speaker');
  }

}
