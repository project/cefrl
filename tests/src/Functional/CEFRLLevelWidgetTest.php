<?php

namespace Drupal\Tests\cefrl\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Cover the field widget functionality.
 *
 * @group cefrl
 */
class CEFRLLevelWidgetTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'node',
    'cefrl'
  ];

  /**
   * A user with permission to create articles.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Set up the test here.
    $this->drupalCreateContentType(['type' => 'article']);
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($this->webUser);

    // Add the CEFRL field to the article content type.
    FieldStorageConfig::create([
      'field_name' => 'field_cefrl',
      'entity_type' => 'node',
      'type' => 'cefrl',
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_cefrl',
      'label' => 'CEFRL',
      'entity_type' => 'node',
      'bundle' => 'article',
      'settings' => [
        'allow_group_selection' => FALSE,
        'include_native_speaker' => FALSE,
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent('field_cefrl', [
        'type' => 'cefrl',
      ])
      ->save();
  }

  /**
   * Tests to confirm the widget is setup and changes according to field config.
   *
   * @covers \Drupal\cefrl\Plugin\Field\FieldWidget\CEFRLLevelWidget::formElement
   */
  public function testFieldWidget() {
    $this->drupalGet('node/add/article');
    $this->assertSession()
      ->fieldValueEquals("field_cefrl[0][level]", '');

    $this->assertSession()
      ->responseContains('<optgroup label="A - Basic user">');
    $this->assertSession()
      ->responseNotContains('<option value="A">');
    $this->assertSession()
      ->responseContains('<option value="A1">');
    $this->assertSession()
      ->responseNotContains('<option value="NS">');

    FieldConfig::loadByName('node', 'article', 'field_cefrl')
      ->set('settings', [
        'allow_group_selection' => TRUE,
        'include_native_speaker' => TRUE,
      ])
      ->save();

    $this->drupalGet('node/add/article');

    $this->assertSession()
      ->responseNotContains('<optgroup label="A - Basic user">');
    $this->assertSession()
      ->responseContains('<option value="A">Any A - Basic user');
    $this->assertSession()
      ->responseContains('<option value="A1">');
    $this->assertSession()
      ->responseContains('<option value="NS">');
  }

}
