<?php

namespace Drupal\Tests\cefrl\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\cefrl\CEFRLLevels;

/**
 * Cover the functionality of the CEFRL levels service.
 *
 * @group cefrl
 */
class CEFRLLevelsTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->cefrlLevels = new CEFRLLevels();
  }

  /**
   * Tests group functionality.
   */
  public function testGroups() {
    $groups = $this->cefrlLevels->getGroups();

    $this->assertContainsOnly('array', $groups);

    $this->assertArrayHasKey('A', $groups);
    $this->assertContains('B1', $groups['B']);
    $this->assertNotContains('B2', $groups['C']);
    $this->assertArrayNotHasKey('NS', $groups);

    $this->assertContains('A1', $this->cefrlLevels->getGroupLevels('A'));
    $this->assertEquals(['C1', 'C2'], $this->cefrlLevels->getGroupLevels('C'));
    $this->assertNotEquals(['B2', 'B1'], $this->cefrlLevels->getGroupLevels('B'));
    $this->assertNull($this->cefrlLevels->getGroupLevels('A2'));
  }

  /**
   * Tests level functionality.
   */
  public function testLevels() {
    $levels = $this->cefrlLevels->getLevels();

    $this->assertContainsOnly('string', $levels);

    $this->assertArrayHasKey('A1', $levels);
    $this->assertEquals('B', $levels['B2']);
    $this->assertNotEquals('B', $levels['C1']);
    $this->assertArrayNotHasKey('NS', $levels);

    $this->assertEquals('A', $this->cefrlLevels->getLevelGroup('A2'));
    $this->assertNotEquals('C', $this->cefrlLevels->getLevelGroup('B1'));
    $this->assertNull($this->cefrlLevels->getLevelGroup('C'));
  }

  /**
   * Tests weight functionality.
   */
  public function testWeights() {
    $weights = $this->cefrlLevels->getWeights();

    $this->assertContainsOnly('integer', $weights);

    $this->assertArrayHasKey('A', $weights);
    $this->assertEquals(220, $weights['B2']);
    $this->assertNotEquals(210, $weights['C1']);

    $this->assertEquals(999, $this->cefrlLevels->getItemWeight('NS'));
    $this->assertNotEquals(400, $this->cefrlLevels->getItemWeight('B1'));
    $this->assertNull($this->cefrlLevels->getItemWeight('X'));
  }

}
