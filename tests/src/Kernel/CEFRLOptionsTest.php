<?php

namespace Drupal\Tests\cefrl\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\cefrl\CEFRLOptions;

/**
 * Cover the functionality of the CEFRL options service.
 *
 * @group cefrl
 */
class CEFRLOptionsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['cefrl'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config_factory = $this->container->get('config.factory');
    $string_translation = $this->container->get('string_translation');
    $cefrl_levels = $this->container->get('cefrl.levels');

    $this->installConfig(['cefrl']);

    $this->cefrlOptions = new CEFRLOptions($config_factory, $string_translation, $cefrl_levels);
  }

  /**
   * Test allowedLevelValues method.
   */
  public function testAllowedLevelValues() {
    $allowed_values = $this->cefrlOptions->allowedLevelValues(FALSE, FALSE);

    $this->assertNotContains('A', $allowed_values);
    $this->assertContains('A1', $allowed_values);
    $this->assertNotContains('NS', $allowed_values);
    $this->assertNotContains('X', $allowed_values);

    $allowed_values = $this->cefrlOptions->allowedLevelValues(TRUE, FALSE);

    $this->assertContains('B', $allowed_values);
    $this->assertContains('B1', $allowed_values);
    $this->assertNotContains('NS', $allowed_values);

    $allowed_values = $this->cefrlOptions->allowedLevelValues(FALSE, TRUE);

    $this->assertNotContains('C', $allowed_values);
    $this->assertContains('C1', $allowed_values);
    $this->assertContains('NS', $allowed_values);

    $allowed_values = $this->cefrlOptions->allowedLevelValues(TRUE, TRUE);

    $this->assertContains('A', $allowed_values);
    $this->assertContains('A2', $allowed_values);
    $this->assertContains('NS', $allowed_values);

    $sorted = $allowed_values;
    sort($sorted);
    $this->assertEquals($allowed_values, $sorted);
  }

  /**
   * Test getOptions method.
   */
  public function testGetOptions() {
    $options = $this->cefrlOptions->getOptions(FALSE, FALSE);
    $this->assertCount(3, $options);
    $this->assertArrayNotHasKey('A', $options);
    $this->assertArrayHasKey('A - Basic user', $options);
    $this->assertArrayHasKey('A1', $options['A - Basic user']);
    $this->assertArrayNotHasKey('A2', $options);
    $this->assertArrayNotHasKey('NS', $options);
    $this->assertArrayNotHasKey('X', $options);

    $options = $this->cefrlOptions->getOptions(TRUE, FALSE);
    $this->assertCount(9, $options);
    $this->assertArrayNotHasKey('B - Independent user', $options);
    $this->assertArrayHasKey('B', $options);
    $this->assertStringStartsWith('Any B', $options['B']);
    $this->assertArrayHasKey('B1', $options);
    $this->assertStringContainsString('B2', $options['B2']);
    $this->assertArrayNotHasKey('NS', $options);

    $options = $this->cefrlOptions->getOptions(FALSE, TRUE);
    $this->assertCount(4, $options);
    $this->assertArrayNotHasKey('C', $options);
    $this->assertArrayHasKey('C - Proficient user', $options);
    $this->assertArrayHasKey('C1', $options['C - Proficient user']);
    $this->assertArrayHasKey('NS', $options);

    $settings = $this->container->get('config.factory')->getEditable('cefrl.settings');
    $definitions = $settings->get('definitions');
    $definitions[8]['label'] = '';
    $settings->set('definitions', $definitions)->save();

    $options = $this->cefrlOptions->getOptions(TRUE, TRUE);
    $this->assertCount(10, $options);
    $this->assertArrayHasKey('C2', $options);
    $this->assertEquals('└── C2', $options['C2']);
    $this->assertArrayHasKey('NS', $options);
    $this->assertEquals('Native speaker', $options['NS']);
  }

  /**
   * Test getWeightOptions method.
   */
  public function testGetWeightOptions() {
    $weight_options = $this->cefrlOptions->getWeightOptions();
    $this->assertCount(3, $weight_options);
    $this->assertArrayNotHasKey('A', $weight_options);
    $this->assertArrayHasKey('A - Basic user', $weight_options);
    $this->assertArrayNotHasKey('A1', $weight_options['A - Basic user']);
    $this->assertArrayHasKey(110, $weight_options['A - Basic user']);
    $this->assertEquals('A1 - Breakthrough', $weight_options['A - Basic user'][110]);
    $this->assertArrayNotHasKey('NS', $weight_options);
    $this->assertArrayNotHasKey(999, $weight_options);
  }

  /**
   * Test getNativeSpeakerOption method.
   */
  public function testGetNativeSpeakerOption() {
    $native_speaker_option = $this->cefrlOptions->getNativeSpeakerOption();
    $this->assertCount(1, $native_speaker_option);
    $this->assertArrayHasKey('NS', $native_speaker_option);
    $this->assertStringContainsStringIgnoringCase('native', $native_speaker_option['NS']);
    $this->assertStringContainsString('speaker', $native_speaker_option['NS']);
  }

  /**
   * Test getLabel method.
   */
  public function testGetLabel() {
    $this->assertEquals('Independent user', $this->cefrlOptions->getLabel('B'));
    $this->assertEquals('Threshold', $this->cefrlOptions->getLabel('B1'));
    $this->assertEquals('Native speaker', $this->cefrlOptions->getLabel('NS'));

    $settings = $this->container->get('config.factory')->getEditable('cefrl.settings');
    $definitions = $settings->get('definitions');
    $this->assertCount(10, $definitions);

    $definitions[5]['label'] = '';
    $settings->set('definitions', $definitions)->save();
    $this->assertEmpty($this->cefrlOptions->getLabel('B2'));
  }

  /**
   * Test getDescription method.
   */
  public function testGetDescription() {
    $this->assertEmpty($this->cefrlOptions->getDescription('C'));
    $this->assertStringStartsWith('Can', $this->cefrlOptions->getDescription('C1'));

    $settings = $this->container->get('config.factory')->getEditable('cefrl.settings');
    $definitions = $settings->get('definitions');
    $this->assertCount(10, $definitions);

    $definitions[8]['description'] = '';
    $settings->set('definitions', $definitions)->save();
    $this->assertEmpty($this->cefrlOptions->getDescription('C2'));

    $definitions[9]['description'] = 'Can do everything!';
    $settings->set('definitions', $definitions)->save();
    $this->assertStringContainsString('everything', $this->cefrlOptions->getDescription('NS'));
  }

}
