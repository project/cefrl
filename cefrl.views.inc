<?php

/**
 * @file
 * Provide views data for cefrl.module.
 */

use Drupal\field\FieldStorageConfigInterface;
use Drupal\cefrl\Plugin\Field\FieldType\CEFRLLevelItem;

/**
 * Implements hook_field_views_data().
 */
function cefrl_field_views_data(FieldStorageConfigInterface $field_storage) {
  $field = $field_storage->getName();

  $property = implode('_', [$field, CEFRLLevelItem::CEFRL_WEIGHT]);

  $data = views_field_default_views_data($field_storage);

  foreach ($data as $key => $value) {
    $data[$key][$property]['filter']['id'] = 'cefrl_weight';
  }

  return $data;
}
