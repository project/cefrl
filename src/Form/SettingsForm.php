<?php

namespace Drupal\cefrl\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cefrl\CEFRLLevelsInterface;
use Drupal\cefrl\CEFRLOptions;
use Drupal\cefrl\CEFRLOptionsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure CEFRL field settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const CEFRL_SETTINGS = CEFRLOptions::CEFRL_SETTINGS;
  const CEFRL_DEFINITIONS = CEFRLOptions::CEFRL_DEFINITIONS;

  const CEFRL_ID = CEFRLOptions::CEFRL_ID;
  const CEFRL_LABEL = CEFRLOptions::CEFRL_LABEL;
  const CEFRL_DESCRIPTION = CEFRLOptions::CEFRL_DESCRIPTION;

  /**
   * The CEFRL levels service.
   *
   * @var \Drupal\cefrl\CEFRLLevelsInterface
   */
  protected $cefrlLevels;

  /**
   * The CEFRL options service.
   *
   * @var \Drupal\cefrl\CEFRLOptionsInterface
   */
  protected $cefrlOptions;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cefrl\CEFRLLevelsInterface $cefrl_levels
   *   The CEFRL levels service.
   * @param \Drupal\cefrl\CEFRLOptionsInterface $cefrl_options
   *   The CEFRL options service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CEFRLLevelsInterface $cefrl_levels,
    CEFRLOptionsInterface $cefrl_options
  ) {
    parent::__construct($config_factory);
    $this->cefrlLevels = $cefrl_levels;
    $this->cefrlOptions = $cefrl_options;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cefrl.levels'),
      $container->get('cefrl.options'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cefrl_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CEFRL_SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_sections = $this->getFormSections();

    $definitions = $this->config(self::CEFRL_SETTINGS)
      ->get(self::CEFRL_DEFINITIONS) ?? [];

    foreach ($form_sections as $key => $section) {
      $title_args = ['%section' => $section];
      $default_label = '';
      $default_description = '';

      foreach ($definitions as $definition) {
        if ($definition[self::CEFRL_ID] === $key) {
          $default_label = $definition[self::CEFRL_LABEL];
          $default_description = $definition[self::CEFRL_DESCRIPTION];
        }
      }

      $form[$key] = [
        '#type' => 'details',
        '#title' => $section,
        '#tree' => TRUE,
        '#open' => (empty($default_label)),
      ];

      $form[$key][self::CEFRL_LABEL] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label for %section', $title_args),
        '#default_value' => $default_label,
      ];

      $form[$key][self::CEFRL_DESCRIPTION] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description for %section', $title_args),
        '#default_value' => $default_description,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->cefrlOptions->getNativeSpeakerOption() as $key => $value) {
      $current = $form_state->getValue($key);

      if (empty($current[self::CEFRL_LABEL])) {
        $form_state->setValue($key, [
          self::CEFRL_LABEL => $value,
          self::CEFRL_DESCRIPTION => $current[self::CEFRL_DESCRIPTION],
        ]);
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_sections = $this->getFormSections();

    $definitions = [];

    foreach ($form_sections as $key => $section) {
      $value = $form_state->getValue($key);
      $definitions[] = [
        self::CEFRL_ID => $key,
        self::CEFRL_LABEL => $value[self::CEFRL_LABEL],
        self::CEFRL_DESCRIPTION => $value[self::CEFRL_DESCRIPTION],
      ];
    }

    $this->config(self::CEFRL_SETTINGS)
      ->set(self::CEFRL_DEFINITIONS, $definitions)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Provide detailed labels for all sections of the form.
   *
   * @return array
   *   Form section labels.
   */
  protected function getFormSections(): array {
    $form_sections = [];
    $groups = $this->cefrlLevels->getGroups();

    foreach ($groups as $group => $levels) {
      $form_sections[$group] = $this->t('Level Group %g', ['%g' => $group]);

      foreach ($levels as $level) {
        $form_sections[$level] = $this->t('Level %l', ['%l' => $level]);
      }
    }

    foreach ($this->cefrlOptions->getNativeSpeakerOption() as $key => $value) {
      $form_sections[$key] = $this->t('%ns pseudo-level', ['%ns' => $value]);
    }

    return $form_sections;
  }

}
