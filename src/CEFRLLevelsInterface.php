<?php

namespace Drupal\cefrl;

/**
 * Defines an interface for a CEFRL levels provider.
 */
interface CEFRLLevelsInterface {

  /**
   * Curated list of CEFRL language level groups and levels contained therein.
   *
   * @return array
   *   Level groups and respective levels.
   */
  public static function groups();

  /**
   * Get CEFRL language level groups.
   *
   * @return array
   *   CEFRL language level groups.
   */
  public function getGroups();

  /**
   * Get CEFRL language levels from a group.
   *
   * @return array|null
   *   Levels contained in a group, or NULL.
   */
  public function getGroupLevels($group);

  /**
   * Curated list of CEFRL language levels and respective groups.
   *
   * @return array
   *   Levels and respective level groups.
   */
  public static function levels();

  /**
   * Get CEFRL language levels.
   *
   * @return array
   *   CEFRL language levels.
   */
  public function getLevels();

  /**
   * Get CEFRL language level group from a level.
   *
   * @return string|null
   *   Group for a given level, or NULL.
   */
  public function getLevelGroup($level);

  /**
   * Numeric weights assigned to level groups, levels and pseudo-levels.
   *
   * @return array
   *   Numeric weights.
   */
  public static function weights();

  /**
   * Get CEFRL weights.
   *
   * @return array
   *   CEFRL weights.
   */
  public function getWeights();

  /**
   * Get CEFRL weight from an item.
   *
   * @return int|null
   *   The weight of an item, or NULL.
   */
  public function getItemWeight($item);

}
