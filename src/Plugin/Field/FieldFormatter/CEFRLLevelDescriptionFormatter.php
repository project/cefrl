<?php

namespace Drupal\cefrl\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'cefrl_description' formatter.
 *
 * @FieldFormatter(
 *   id = "cefrl_description",
 *   label = @Translation("Description"),
 *   field_types = {"cefrl"}
 * )
 */
class CEFRLLevelDescriptionFormatter extends CEFRLLevelDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $empty = $this->t('No description available.');

    foreach ($items as $delta => $item) {
      if ($item->level) {
        $description = $this->cefrlOptions->getDescription($item->level);

        $element[$delta] = [
          '#theme' => 'cefrl_description',
          '#label' => $this->getProcessedLabel($item->level),
          '#description' => (empty($description)) ? $empty : $description,
        ];
      }
    }

    return $element;
  }

}
