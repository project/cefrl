<?php

namespace Drupal\cefrl\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\cefrl\CEFRLLevels;
use Drupal\cefrl\CEFRLOptions;

/**
 * Defines the 'cefrl' field type.
 *
 * @FieldType(
 *   id = "cefrl",
 *   label = @Translation("CEFRL level"),
 *   category = @Translation("General"),
 *   default_widget = "cefrl",
 *   default_formatter = "cefrl_default"
 * )
 */
class CEFRLLevelItem extends FieldItemBase {

  const CEFRL_LEVEL = 'level';
  const CEFRL_WEIGHT = 'weight';

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      CEFRLOptions::CEFRL_ALLOW_GROUPS => FALSE,
      CEFRLOptions::CEFRL_INCLUDE_NATIVE => FALSE,
    ];

    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $element[CEFRLOptions::CEFRL_ALLOW_GROUPS] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow selection of level group'),
      '#default_value' => $settings[CEFRLOptions::CEFRL_ALLOW_GROUPS],
    ];

    $element[CEFRLOptions::CEFRL_INCLUDE_NATIVE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Native speaker option'),
      '#default_value' => $settings[CEFRLOptions::CEFRL_INCLUDE_NATIVE],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->level !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties[self::CEFRL_LEVEL] = DataDefinition::create('string')
      ->setLabel(t('Level'))
      ->setRequired(TRUE);

    $properties[self::CEFRL_WEIGHT] = DataDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\cefrl\CEFRLWeight');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $settings = $this->getSettings();
    $allow_groups = (bool) $settings[CEFRLOptions::CEFRL_ALLOW_GROUPS];
    $include_native = (bool) $settings[CEFRLOptions::CEFRL_INCLUDE_NATIVE];

    $constraints = parent::getConstraints();

    $options[self::CEFRL_LEVEL] = [
      'AllowedValues' => \Drupal::service('cefrl.options')
        ->allowedLevelValues($allow_groups, $include_native),
      'NotBlank' => [],
    ];

    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create('ComplexData', $options);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      self::CEFRL_LEVEL => [
        'type' => 'varchar',
        'length' => 255,
      ],
      self::CEFRL_WEIGHT => [
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'small',
        'not null' => TRUE,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $weights = CEFRLLevels::weights();

    $random = array_rand($weights);

    $values[self::CEFRL_LEVEL] = $random;
    $values[self::CEFRL_WEIGHT] = $weights[$random];

    return $values;
  }

}
