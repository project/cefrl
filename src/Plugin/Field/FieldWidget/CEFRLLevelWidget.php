<?php

namespace Drupal\cefrl\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\cefrl\CEFRLOptions;
use Drupal\cefrl\CEFRLOptionsInterface;
use Drupal\cefrl\Plugin\Field\FieldType\CEFRLLevelItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'cefrl' field widget.
 *
 * @FieldWidget(
 *   id = "cefrl",
 *   label = @Translation("CEFRL level"),
 *   field_types = {"cefrl"},
 * )
 */
class CEFRLLevelWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The CEFRL Options service.
   *
   * @var \Drupal\cefrl\CEFRLOptionsInterface
   */
  protected $cefrlOptions;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    CEFRLOptionsInterface $cefrl_options
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->cefrlOptions = $cefrl_options;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('cefrl.options')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_settings = $items->getSettings();

    $allow_groups = $field_settings[CEFRLOptions::CEFRL_ALLOW_GROUPS];
    $include_native = $field_settings[CEFRLOptions::CEFRL_INCLUDE_NATIVE];

    $options = $this->cefrlOptions->getOptions($allow_groups, $include_native);

    $element[CEFRLLevelItem::CEFRL_LEVEL] = [
      '#type' => 'select',
      '#options' => ['' => $this->t('- Select a value -')] + $options,
      '#default_value' => $items[$delta]->level ?? NULL,
    ];

    $cardinality = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    // If cardinality is 1, ensure a proper label is output for the field.
    if ($cardinality === 1) {
      $element[CEFRLLevelItem::CEFRL_LEVEL]['#title'] = $element['#title'];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if ($value[CEFRLLevelItem::CEFRL_LEVEL] === '') {
        $values[$delta][CEFRLLevelItem::CEFRL_LEVEL] = NULL;
      }
    }
    return $values;
  }

}
