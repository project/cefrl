<?php

namespace Drupal\cefrl\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\NumericFilter;

/**
 * CEFRL weight filter to handle greater than/less than filters.
 *
 * Changes NumericFilter to use select elements instead of textfields.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("cefrl_weight")
 */
class CEFRLWeightFilter extends NumericFilter {

  /**
   * Remove the placeholder options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    unset($options['expose']['contains']['placeholder']);
    unset($options['expose']['contains']['min_placeholder']);
    unset($options['expose']['contains']['max_placeholder']);

    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Remove the placeholder options.
   */
  public function defaultExposeOptions() {
    parent::defaultExposeOptions();

    unset($this->options['expose']['min_placeholder']);
    unset($this->options['expose']['max_placeholder']);
    unset($this->options['expose']['placeholder']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);

    unset($form['expose']['min_placeholder']);
    unset($form['expose']['max_placeholder']);
    unset($form['expose']['placeholder']);
  }

  /**
   * Remove the RegEx operator.
   */
  public function operators() {
    $operators = parent::operators();

    unset($operators['regular_expression']);

    return $operators;
  }

  /**
   * Provide select elements instead of textfields and remove placeholders.
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $weight_options = \Drupal::service('cefrl.options')->getWeightOptions();

    $form['value']['#tree'] = TRUE;

    // We have to make some choices when creating this as an exposed
    // filter form. For example, if the operator is locked and thus
    // not rendered, we can't render dependencies; instead we only
    // render the form items we need.
    $which = 'all';
    if (!empty($form['operator'])) {
      $source = ':input[name="options[operator]"]';
    }

    if ($exposed = $form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];

      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator_id'])) {
        // Exposed and locked.
        $which = in_array($this->operator, $this->operatorValues(2)) ? 'minmax' : 'value';
      }
      else {
        $source = ':input[name="' . $this->options['expose']['operator_id'] . '"]';
      }
    }

    $user_input = $form_state->getUserInput();
    if ($which == 'all') {
      $form['value']['value'] = [
        '#type' => 'select',
        '#options' => $weight_options,
        '#title' => !$exposed ? $this->t('Value') : '',
        '#default_value' => $this->value['value'],
      ];
      // Setup #states for all operators with one value.
      foreach ($this->operatorValues(1) as $operator) {
        $form['value']['value']['#states']['visible'][] = [
          $source => ['value' => $operator],
        ];
      }
      if ($exposed && !isset($user_input[$identifier]['value'])) {
        $user_input[$identifier]['value'] = $this->value['value'];
        $form_state->setUserInput($user_input);
      }
    }
    elseif ($which == 'value') {
      // When exposed we drop the value-value and just do value if
      // the operator is locked.
      $form['value'] = [
        '#type' => 'select',
        '#options' => $weight_options,
        '#title' => !$exposed ? $this->t('Value') : '',
        '#default_value' => $this->value['value'],
      ];
      if ($exposed && !isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value['value'];
        $form_state->setUserInput($user_input);
      }
    }

    // Minimum and maximum form fields are associated to some specific operators
    // like 'between'. Ensure that min and max fields are only visible if
    // the associated operator is not excluded from the operator list.
    $two_value_operators_available = ($which == 'all' || $which == 'minmax');

    if (!empty($this->options['expose']['operator_limit_selection']) &&
        !empty($this->options['expose']['operator_list'])) {
      $two_value_operators_available = FALSE;
      foreach ($this->options['expose']['operator_list'] as $operator) {
        if (in_array($operator, $this->operatorValues(2), TRUE)) {
          $two_value_operators_available = TRUE;
          break;
        }
      }
    }

    if ($two_value_operators_available) {
      $form['value']['min'] = [
        '#type' => 'select',
        '#options' => $weight_options,
        '#title' => $this->t('Min'),
        '#default_value' => $this->value['min'],
      ];
      $form['value']['max'] = [
        '#type' => 'select',
        '#options' => $weight_options,
        '#title' => $this->t('Max'),
        '#default_value' => $this->value['max'],
      ];
      if ($which == 'all') {
        $states = [];
        // Setup #states for all operators with two values.
        foreach ($this->operatorValues(2) as $operator) {
          $states['#states']['visible'][] = [
            $source => ['value' => $operator],
          ];
        }
        $form['value']['min'] += $states;
        $form['value']['max'] += $states;
      }
      if ($exposed && !isset($user_input[$identifier]['min'])) {
        $user_input[$identifier]['min'] = $this->value['min'];
      }
      if ($exposed && !isset($user_input[$identifier]['max'])) {
        $user_input[$identifier]['max'] = $this->value['max'];
      }

      if (!isset($form['value'])) {
        // Ensure there is something in the 'value'.
        $form['value'] = [
          '#type' => 'value',
          '#value' => NULL,
        ];
      }
    }
  }

  /**
   * Display labels instead of numeric weights in summary.
   */
  public function adminSummary() {
    if ($this->isAGroup()) {
      return $this->t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }

    $weight_options = \Drupal::service('cefrl.options')->getWeightOptions();
    foreach ($weight_options as $levels) {
      foreach ($levels as $weight => $label) {
        $flat_options[$weight] = $label;
      }
    }

    $options = $this->operatorOptions('short');
    $output = $options[$this->operator];
    if (in_array($this->operator, $this->operatorValues(2))) {
      $output .= ' ' . $this->t('@min and @max', [
        '@min' => $flat_options[$this->value['min']],
        '@max' => $flat_options[$this->value['max']],
      ]);
    }
    elseif (in_array($this->operator, $this->operatorValues(1))) {
      $output .= ' ' . $flat_options[$this->value['value']];
    }
    return $output;
  }

}
