<?php

namespace Drupal\cefrl;

/**
 * Provides CEFRL groups, levels and assigned weights.
 */
class CEFRLLevels implements CEFRLLevelsInterface {

  // Groups and levels.
  const CEFRL_A = 'A';
  const CEFRL_A1 = 'A1';
  const CEFRL_A2 = 'A2';
  const CEFRL_B = 'B';
  const CEFRL_B1 = 'B1';
  const CEFRL_B2 = 'B2';
  const CEFRL_C = 'C';
  const CEFRL_C1 = 'C1';
  const CEFRL_C2 = 'C2';
  // Pseudo-level for Native speaker.
  const CEFRL_NS = 'NS';

  /**
   * CEFRL language level groups.
   *
   * @var array
   */
  protected $groups;

  /**
   * CEFRL language levels.
   *
   * @var array
   */
  protected $levels;

  /**
   * CEFRL weights.
   *
   * @var array
   */
  protected $weights;

  /**
   * Curated list of CEFRL language level groups and levels contained therein.
   *
   * @return array
   *   Level groups and respective levels.
   */
  public static function groups(): array {
    return [
      self::CEFRL_A => [self::CEFRL_A1, self::CEFRL_A2],
      self::CEFRL_B => [self::CEFRL_B1, self::CEFRL_B2],
      self::CEFRL_C => [self::CEFRL_C1, self::CEFRL_C2],
    ];
  }

  /**
   * Get CEFRL language level groups.
   *
   * @return array
   *   CEFRL language level groups.
   */
  public function getGroups(): array {
    if (!isset($this->groups)) {
      $this->groups = static::groups();
    }

    return $this->groups;
  }

  /**
   * Get CEFRL language levels from a group.
   *
   * @return array|null
   *   Levels contained in a group, or NULL.
   */
  public function getGroupLevels($group): ?array {
    $groups = $this->getGroups();

    return $groups[$group] ?? NULL;
  }

  /**
   * Curated list of CEFRL language levels and respective groups.
   *
   * @return array
   *   Levels and respective level groups.
   */
  public static function levels(): array {
    return [
      self::CEFRL_A1 => self::CEFRL_A,
      self::CEFRL_A2 => self::CEFRL_A,
      self::CEFRL_B1 => self::CEFRL_B,
      self::CEFRL_B2 => self::CEFRL_B,
      self::CEFRL_C1 => self::CEFRL_C,
      self::CEFRL_C2 => self::CEFRL_C,
    ];
  }

  /**
   * Get CEFRL language levels.
   *
   * @return array
   *   CEFRL language levels.
   */
  public function getLevels(): array {
    if (!isset($this->levels)) {
      $this->levels = static::levels();
    }

    return $this->levels;
  }

  /**
   * Get CEFRL language level group from a level.
   *
   * @return string|null
   *   Group for a given level, or NULL.
   */
  public function getLevelGroup($level): ?string {
    $levels = $this->getLevels();

    return $levels[$level] ?? NULL;
  }

  /**
   * Numeric weights assigned to level groups, levels and pseudo-levels.
   *
   * @return array
   *   Numeric weights.
   */
  public static function weights(): array {
    return [
      self::CEFRL_A => 100,
      self::CEFRL_A1 => 110,
      self::CEFRL_A2 => 120,
      self::CEFRL_B => 200,
      self::CEFRL_B1 => 210,
      self::CEFRL_B2 => 220,
      self::CEFRL_C => 300,
      self::CEFRL_C1 => 310,
      self::CEFRL_C2 => 320,
      self::CEFRL_NS => 999,
    ];
  }

  /**
   * Get CEFRL weights.
   *
   * @return array
   *   CEFRL weights.
   */
  public function getWeights(): array {
    if (!isset($this->weights)) {
      $this->weights = static::weights();
    }

    return $this->weights;
  }

  /**
   * Get CEFRL weight from an item.
   *
   * @return int|null
   *   The weight of an item, or NULL.
   */
  public function getItemWeight($item): ?int {
    $weights = $this->getWeights();

    return $weights[$item] ?? NULL;
  }

}
