<?php

namespace Drupal\cefrl;

/**
 * Defines an interface for a CEFRL options provider.
 */
interface CEFRLOptionsInterface {

  /**
   * Provides the allowed values for the CEFRL level.
   *
   * @param bool $allow_groups
   *   Indicates whether level groups are allowed.
   * @param bool $include_native
   *   Indicates whether the Native speaker pseudo-level is allowed.
   *
   * @return array
   *   The allowed values for the CEFRL level.
   */
  public function allowedLevelValues(bool $allow_groups = FALSE, bool $include_native = FALSE);

  /**
   * Provides the options for a field widget.
   *
   * @param bool $allow_groups
   *   Indicates whether level groups are allowed.
   * @param bool $include_native
   *   Indicates whether the Native speaker pseudo-level is allowed.
   *
   * @return array
   *   The options for a field widget.
   */
  public function getOptions(bool $allow_groups = FALSE, bool $include_native = FALSE);

  /**
   * Provides the grouped CEFRL levels as options, indexed by weight.
   *
   * @return array
   *   The weight indexed options.
   */
  public function getWeightOptions();

  /**
   * Provides the 'Native speaker' pseudo-level option.
   *
   * @return array
   *   The 'Native speaker' pseudo-level option.
   */
  public function getNativeSpeakerOption();

  /**
   * Provides the label for a level or group as defined in configuration.
   *
   * If no label is defined in configuration, the original key is returned.
   *
   * If the key is not found in configuration, NULL is returned.
   *
   * @param string $key
   *   The key of the item (level or group).
   *
   * @return string|null
   *   The label for the item, or NULL.
   */
  public function getLabel(string $key);

  /**
   * Provides the description for a level or group as defined in configuration.
   *
   * If no description is defined in configuration, an empty string is returned.
   *
   * If the key is not found in configuration, NULL is returned.
   *
   * @param string $key
   *   The key of the item (level or group).
   *
   * @return string|null
   *   The description for the item, or NULL.
   */
  public function getDescription(string $key);

}
