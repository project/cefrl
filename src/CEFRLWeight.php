<?php

namespace Drupal\cefrl;

use Drupal\Core\TypedData\TypedData;

/**
 * A computed property containing the weight of an item.
 */
class CEFRLWeight extends TypedData {

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $level = $this->getParent()->get('level')->getValue();

    $weights = CEFRLLevels::weights();

    return $weights[$level] ?? 0;
  }

}
